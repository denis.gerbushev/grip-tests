import {GRIP_API_V3_URL, GRIP_EMAIL, GRIP_PASSWORD} from "../../../utils/defaultRequests"

const request = require('supertest')
const app = request(GRIP_API_V3_URL)

describe('API /auth/login endpoint', () => {
    [
        [GRIP_EMAIL, GRIP_PASSWORD, 200],
        ['wrong@email.com', 'wrong-password', 401],
        ['test@test.com', '', 400],
        ['', 'test', 400],
        ['', '', 400],
        ['SELECT * FROM', 'test', 400],
    ].forEach(([email, password, responseCode]) => {
        it(`POST /auth/login should has status ${responseCode} with email: ${email}, password: ${password}`, async () => {
            const response = await app.post('/auth/login').send({'email': email, 'password': password})
            expect(response.status).toBe(responseCode)
        })
    })
})

