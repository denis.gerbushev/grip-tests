import {GRIP_API_URL, GRIP_EMAIL} from "../../../utils/defaultRequests"

const request = require('supertest')
const app = request(GRIP_API_URL)

describe('API /login/ssoconfiguration endpoint', () => {
    [
        [GRIP_EMAIL, 200],
        ['', 400],
        ['SELECT * FROM', 400],
    ].forEach(([email, responseCode]) => {
        it(`POST /login/ssoconfiguration should has status ${responseCode} with email: ${email}`, async () => {
            const response = await app.post('/login/ssoconfiguration').send({'email': email})
            expect(response.status).toBe(responseCode)
        })
    })
})