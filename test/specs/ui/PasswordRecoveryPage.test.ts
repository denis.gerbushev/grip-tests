import LoginPage from '../../../pageobjects/login.page'
import PasswordRecoveryPage, {emailPlaceholderText, submitMessageText} from '../../../pageobjects/passwordRecovery.page'
import {GRIP_EMAIL} from "../../../utils/defaultRequests"
import allureReporter from "@wdio/allure-reporter"

describe('UI Password recovery page', () => {
    it('should have the Grip logo', async () => {
        allureReporter.addTestId('GRIP-015')
        await PasswordRecoveryPage.open(null)
        await expect(PasswordRecoveryPage.gripLogo).toBeDisplayed()
    })
    it('should have the text description', async () => {
        allureReporter.addTestId('GRIP-016')
        await PasswordRecoveryPage.open(null)
        await expect(PasswordRecoveryPage.textDescription).toBeDisplayed()
    })
    it('should have the email field placeholder', async () => {
        allureReporter.addTestId('GRIP-017')
        await PasswordRecoveryPage.open(null)
        await expect(PasswordRecoveryPage.emailPlaceholder).toHaveText(emailPlaceholderText)
    })

    describe('The "Back to login" link', () => {
            it('should open the "Login" page', async () => {
                allureReporter.addTestId('GRIP-018')
                await PasswordRecoveryPage.open(null)
                await PasswordRecoveryPage.linkBackToLogin.click()
                await expect(LoginPage.header).toBeDisplayed()
            })
        }
    )

    it('should display success message for valid email', async () => {
        allureReporter.addTestId('GRIP-019')
        await PasswordRecoveryPage.open(null)
        await PasswordRecoveryPage.typeEmail(GRIP_EMAIL)
        await PasswordRecoveryPage.btnSendTheLink.click()
        console.log(await PasswordRecoveryPage.submitMessage.getText())
        await expect(PasswordRecoveryPage.submitMessage).toHaveText(submitMessageText)
    })
    describe('Email field', () => {
        [
            ['test'],
            ['test@test'],
            ['test@test.'],
            ['@test.com'],
            ['@.com'],
            ['test@.com'],
            ['test@@test.com'],
            ['te$t@test.com'],
        ].forEach(([email]) => {
            it(`should have validation error "Invalid email address" with ${email}`, async () => {
                allureReporter.addTestId('GRIP-020')
                await PasswordRecoveryPage.open(null)
                await PasswordRecoveryPage.typeEmail(email)
                await expect(PasswordRecoveryPage.emailValidationErrorMessage).toHaveText('Invalid email address')
            })
        })
    })
    it('should have the email field value for opening with a parameter email', async () => {
        allureReporter.addTestId('GRIP-021')
        await PasswordRecoveryPage.open(GRIP_EMAIL)
        await expect(PasswordRecoveryPage.inputEmail).toHaveValue(GRIP_EMAIL)
    })

    describe('The "Send the link" button', () => {
            it('should be disabled when the email field is empty', async () => {
                allureReporter.addTestId('GRIP-022')
                await PasswordRecoveryPage.open(null)
                await expect(PasswordRecoveryPage.btnSendTheLink).toBeDisabled()
            })
        }
    )
})


