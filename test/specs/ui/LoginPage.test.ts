import LoginPage, {emailPlaceholderText, passwordPlaceholderText} from '../../../pageobjects/login.page'
import MainPage from '../../../pageobjects/main.page'
import PasswordRecoveryPage from '../../../pageobjects/passwordRecovery.page'
import SignupPage from '../../../pageobjects/signup.page'
import {GRIP_EMAIL, GRIP_PASSWORD} from "../../../utils/defaultRequests"
import allureReporter from '@wdio/allure-reporter'

describe('UI Login page', () => {
    it('should have the Grip logo', async () => {
        allureReporter.addTestId('GRIP-001')
        await LoginPage.open()
        await expect(LoginPage.gripLogo).toBeDisplayed()
    })
    it('should have the text description', async () => {
        allureReporter.addTestId('GRIP-002')
        await LoginPage.open()
        await expect(LoginPage.textDescription).toBeDisplayed()
    })
    it('should have the header "Log in"', async () => {
        allureReporter.addTestId('GRIP-003')
        await LoginPage.open()
        await expect(LoginPage.header).toBeDisplayed()
    })
    it('should have the email field placeholder', async () => {
        allureReporter.addTestId('GRIP-004')
        await LoginPage.open()
        await expect(LoginPage.emailPlaceholder).toHaveText(emailPlaceholderText)
    })
    it('should have the password field placeholder', async () => {
        allureReporter.addTestId('GRIP-005')
        await LoginPage.open()
        await LoginPage.typeEmail('test@test.com')
        await LoginPage.btnNext.click()
        await expect(LoginPage.passwordPlaceholder).toHaveText(passwordPlaceholderText)
    })
    it('should success login with valid credentials', async () => {
        allureReporter.addTestId('GRIP-006')
        await LoginPage.open()
        await LoginPage.login(GRIP_EMAIL, GRIP_PASSWORD)
        await expect(MainPage.header).toBeDisplayed()
    })
    it('should display the error "Invalid email or password" with wrong credentials', async () => {
        allureReporter.addTestId('GRIP-007')
        await LoginPage.open()
        await LoginPage.login('wrong@email.com', 'wrong-password')
        await expect(LoginPage.passwordValidationErrorMessage).toHaveText('Invalid email or password')
    })

    describe('The "Sign up" button', () => {
            it('should open the "Sign up" page', async () => {
                allureReporter.addTestId('GRIP-008')
                await LoginPage.open()
                await LoginPage.btnSignup.click()
                await expect(SignupPage.header).toBeDisplayed()
            })
        }
    )

    describe('The "Forgot your password?" link', () => {
            it('should open the "Password recovery" page', async () => {
                allureReporter.addTestId('GRIP-009')
                await LoginPage.open()
                await LoginPage.typeEmail('test@test.com')
                await LoginPage.btnNext.click()
                await LoginPage.linkPasswordRecovery.click()
                await expect(PasswordRecoveryPage.header).toBeDisplayed()
            })
        }
    )

    describe('Email field', () => {
        [
            ['test'],
            ['test@test'],
            ['test@test.'],
            ['@test.com'],
            ['@.com'],
            ['test@.com'],
            ['test@@test.com'],
            ['te$t@test.com'],
        ].forEach(([email]) => {
            it(`should have validation error "Invalid email address" with ${email}`, async () => {
                allureReporter.addTestId('GRIP-010')
                await LoginPage.open()
                await LoginPage.typeEmail(email)
                await expect(LoginPage.emailValidationErrorMessage).toHaveText('Invalid email address')
            })
        })
        it('should be disabled after click Next button', async () => {
            allureReporter.addTestId('GRIP-011')
            await LoginPage.open()
            await LoginPage.typeEmail('test@test.com')
            await LoginPage.btnNext.click()
            await expect(LoginPage.inputEmail).toBeDisabled()
        })
        it('should have an ability to be changed after click Next button', async () => {
            allureReporter.addTestId('GRIP-012')
            await LoginPage.open()
            await LoginPage.typeEmail('test@test.com')
            await LoginPage.btnNext.click()
            await LoginPage.clickOnEmailField()
            await expect(LoginPage.inputEmail).toBeEnabled()
        })
    })
    describe('The "Next" button', () => {
            it('should be disabled before any actions', async () => {
                allureReporter.addTestId('GRIP-013')
                await LoginPage.open()
                await expect(LoginPage.btnNext).toBeDisabled()
            })
        it('should be disabled before any actions', async () => {
            allureReporter.addTestId('GRIP-014')
            await LoginPage.open()
            await LoginPage.typeEmail('test')
            await expect(LoginPage.btnNext).toBeDisabled()
        })
        }
    )
})


