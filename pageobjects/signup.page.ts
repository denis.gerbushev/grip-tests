import {ChainablePromiseElement} from 'webdriverio'

import Page from './page'

// Locators
const header = 'h1=Sign up'

class SignupPage extends Page {
    public get header(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(header)
    }
}

export default new SignupPage()
