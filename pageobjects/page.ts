export default class Page {
    public open(path: string): Promise<string> {
        return browser.url(`.${path}`)
    }

    public sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms))
    }
}
