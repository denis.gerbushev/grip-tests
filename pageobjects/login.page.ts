import {ChainablePromiseElement} from 'webdriverio'

import Page from './page'

export const emailPlaceholderText = 'Email'
export const passwordPlaceholderText = 'Password'

// Locators
const header = 'h1=Log in'
const gripLogo = 'div[data-test-component="gripLogo"]'
const textDescription = '//div[text()="Web-based software that automatically generates visual content."]'
const emailValidationErrorMessage = '//div[@data-test-component="loginPage__email field"]//div[contains(@data-test-component, "errorMessage__text")]';
const emailPlaceholder = '//div[@data-test-component="loginPage__email field"]//div[contains(@data-test-component, "placeholder")]'
const passwordValidationErrorMessage = '//div[@data-test-component="loginPage__password field"]//div[contains(@data-test-component, "errorMessage__text")]'
const passwordPlaceholder = '//div[@data-test-component="loginPage__password field"]//div[contains(@data-test-component, "placeholder")]'
const parentDivOfEmail = '//input[@name="email"]/parent::div'
const inputEmail = 'input[name="email"]'
const inputPassword = 'input[name="password"]'
const btnNext = 'button[title="Next"]'
const btnSignup = 'button[title="Sign up"]'
const linkPasswordRecovery = '//a[contains(@data-test-component, "loginPage__passwordRecovery")]'
const btnSubmit = 'button[title="Log in"]'

class LoginPage extends Page {
    public get header(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(header)
    }

    public get gripLogo(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(gripLogo)
    }

    public get textDescription(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(textDescription)
    }

    public get emailValidationErrorMessage(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(emailValidationErrorMessage)
    }

    public get emailPlaceholder(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(emailPlaceholder)
    }

    public get passwordValidationErrorMessage(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(passwordValidationErrorMessage)
    }

    public get passwordPlaceholder(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(passwordPlaceholder)
    }

    public get parentDivOfEmail(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(parentDivOfEmail)
    }

    public get inputEmail(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(inputEmail)
    }

    public get inputPassword(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(inputPassword)
    }

    public get btnNext(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(btnNext)
    }

    public get btnSignup(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(btnSignup)
    }

    public get linkPasswordRecovery(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(linkPasswordRecovery)
    }

    public get btnSubmit(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(btnSubmit)
    }

    public async login(email: string, password: string): Promise<void> {
        await this.inputEmail.setValue(email)
        await this.btnNext.click()
        await this.inputPassword.setValue(password)
        await this.btnSubmit.click()
    }

    public async typeEmail(email: string): Promise<void> {
        await this.inputEmail.setValue(email)
        await this.inputEmail.setValue(['Tab'])
    }

    public async clickOnEmailField(): Promise<void> {
        await this.sleep(1000)
        await this.parentDivOfEmail.click()
    }

    public open(): Promise<string> {
        return super.open('/login')
    }
}

export default new LoginPage()
