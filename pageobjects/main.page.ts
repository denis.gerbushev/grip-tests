import {ChainablePromiseElement} from 'webdriverio'

import Page from './page'

// Locators
const header = 'h1=Welcome Grip main page'

class MainPage extends Page {
    public get header(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(header)
    }
}

export default new MainPage()
