import {ChainablePromiseElement} from 'webdriverio'
import Page from './page'

export const submitMessageText = 'If we find a corresponding account to the email you have specified, we will send ' +
    'you a password reset email shortly.\n' +
    'Please note that the password reset link expires within a few hours and do make sure your spam folder in case ' +
    'you don\'t receive the email shortly.\n' +
    'Thank you.'

export const emailPlaceholderText = 'Please enter your email'

// Locators
const header = 'h1=Password recovery'
const gripLogo = 'div[data-test-component="gripLogo"]'
const textDescription = '//div[text()="Web-based software that automatically generates visual content."]'
const btnSendTheLink = 'button[title="Send the link"]'
const linkBackToLogin = '//a[contains(@data-test-component,"passwordRecoveryPage__goBack")]'
const emailPlaceholder = '//div[@data-test-component="passwordRecoveryPage__email field"]//div[contains(@data-test-component, "placeholder")]'
const inputEmail = 'input[name="email"]'
const submitMessage = '//div[@data-test-component="passwordRecoveryPage__submitMessage"]'
const emailValidationErrorMessage = '//div[@data-test-component="passwordRecoveryPage__email field"]//div[contains(@data-test-component, "errorMessage__text")]';

class PasswordRecoveryPage extends Page {
    public get header(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(header)
    }

    public get gripLogo(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(gripLogo)
    }

    public get textDescription(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(textDescription)
    }

    public get btnSendTheLink(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(btnSendTheLink)
    }

    public get linkBackToLogin(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(linkBackToLogin)
    }

    public get emailPlaceholder(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(emailPlaceholder)
    }

    public get inputEmail(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(inputEmail)
    }

    public async typeEmail(email: string): Promise<void> {
        await this.inputEmail.setValue(email)
        await this.inputEmail.setValue(['Tab'])
    }

    public get submitMessage(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(submitMessage)
    }

    public get emailValidationErrorMessage(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $(emailValidationErrorMessage)
    }

    public open(parameter): Promise<string> {
        if (parameter) {
            return super.open('/password-recovery?email=' + parameter)
        } else {
            return super.open('/password-recovery')
        }
    }
}

export default new PasswordRecoveryPage()
