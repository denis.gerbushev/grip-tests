# grip-tests

Automation tests framework for the Grip web application.
- TypeScript (https://www.typescriptlang.org/)
- WebdriverIO (https://webdriver.io/)
- Mocha (https://mochajs.org/)
- SuperTest (https://www.npmjs.com/package/supertest)


## Getting started

1) Download and install NodeJS (https://nodejs.org/en/download/)
2) Install Chrome browser (https://www.google.com/chrome/)
3) Clone the repository:
```
git clone https://gitlab.com/denis.gerbushev/grip-tests.git
```
4) Run the following commands to install dependencies:
```
cd grip-tests
npm install
```
That's enough!
## Run tests
```
GRIP_HOST=https://demo-v2.grip.tools npm run test
```
You can run tests in another environment. Set GRIP_HOST='GRIP_APPLICATION_HOST'.

## Build report
All testing results are saved in ./allure-report directory, using Allure.
You can build the report:
1) Install Java (https://www.java.com/en/download/help/windows_manual_download.html)
2) Install Allure application (https://docs.qameta.io/allure/#_installing_a_commandline)
3) In the root of this repository run:
```
allure serve
```
The Allure will be opened in your browser.

## Repository structure

- pageobjects - PageObjects model for UI tests
- utils/defaultRequests.ts - host initialisation, api paths, some credentials for tests
- package.json - packages configuration and run templates
- tsconfig.json - compiler configuration
- wdio.conf.ts - webdriverIO configuration
- test/specs/api - API tests
- test/specs/ui - UI tests
