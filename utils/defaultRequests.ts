const GRIP_API_PATH = '/api'
const GRIP_API_V3_PATH = '/api/v3'

export const GRIP_API_V3_URL = process.env.GRIP_HOST + GRIP_API_V3_PATH
export const GRIP_API_URL = process.env.GRIP_HOST + GRIP_API_PATH

export const GRIP_EMAIL = 'valid@email.com'
export const GRIP_PASSWORD = 'valid-password'